import readXlsxFile from 'read-excel-file';

import { readExcelFileLotteryResultsSchema, MAIN_BALLS, FIELDS } from './constants';

/**
 * @typedef {Object[]} LotteryResults 
 */

/**
 * @param   {Object} file
 * @returns {LotteryResults}
 */
export const parseLotteryResultsXlsx = async (file) => {
    try {
        const { rows, errors } = await readXlsxFile(file, { schema: readExcelFileLotteryResultsSchema });

        return { rows, errors };
    } catch (error) {
        return { errors: [error] };
    }
};

/**
 * @typedef {Object} LotteryBallLastDraw
 * @prop    {number} draw
 * @prop    {string} date
 */

/**
 * @typedef {Object} LotteryBallStatsEntity
 * @prop    {number} key
 * @prop    {number} count
 * @prop    {LotteryBallLastDraw} last
 */

/**
 * 
 * @typedef {Object<string, LotteryBallStatsEntity>} LotteryBallsStats 
 */

/**
 * 
 * @param   {LotteryResults} lotteryResults
 * @returns {LotteryBallsStats}
 */
export const createLotteryBallsStats = (lotteryResults = []) => lotteryResults.reduce(
    (acc, lotteryDrawData) => {
      MAIN_BALLS.forEach(
        (ball) => {
          const ballNumber = lotteryDrawData[ball];

          if (acc[ballNumber]) {
            acc[ballNumber].count += 1;
          } else {
            acc[ballNumber] = { count: 1 };
          }

          acc[ballNumber].key = ballNumber;
          acc[ballNumber].last = {
            draw: lotteryDrawData[FIELDS.lotteryDraw],
            date: lotteryDrawData[FIELDS.date],
          };
        },

      );

      return acc;
    },
    Object.create(null),
  );
