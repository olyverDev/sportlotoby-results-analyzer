export const FIELDS = {
    lotteryDraw: 'lotteryDraw',
    date: 'date',
    ball1: 'ball1',
    ball2: 'ball2',
    ball3: 'ball3',
    ball4: 'ball4',
    ball5: 'ball5',
    ball6: 'ball6',
  };

  export const MAIN_BALLS = [
    FIELDS.ball1,
    FIELDS.ball2,
    FIELDS.ball3,
    FIELDS.ball4,
    FIELDS.ball5,
  ];
  
  export const FIELDS_SETTINGS = {
    [FIELDS.lotteryDraw]: {
      ID: FIELDS.lotteryDraw,
      rawID: 'Тираж',
      type: Number,
    },
    [FIELDS.date]: {
      ID: FIELDS.date,
      rawID: 'Дата',
      type: String,
    },
    [FIELDS.ball1]: {
      ID: FIELDS.ball1,
      rawID: 'Шар 1',
      type: Number,
    },
    [FIELDS.ball2]: {
      ID: FIELDS.ball2,
      rawID: 'Шар 2',
      type: Number,
    },
    [FIELDS.ball3]: {
      ID: FIELDS.ball3,
      rawID: 'Шар 3',
      type: Number,
    },
    [FIELDS.ball4]: {
      ID: FIELDS.ball4,
      rawID: 'Шар 4',
      type: Number,
    },
    [FIELDS.ball5]: {
      ID: FIELDS.ball5,
      rawID: 'Шар 5',
      type: Number,
    },
    [FIELDS.ball6]: {
      ID: FIELDS.ball6,
      rawID: 'Шар 6',
      type: Number,
    }
  };
  
  export const readExcelFileLotteryResultsSchema = Object.values(FIELDS_SETTINGS).reduce(
    (acc, { ID, rawID, type, ...other }) => {
      acc[rawID] = {
        ...other,
        prop: ID,
        type,
      };
      return acc;
    },
    Object.create(null),
  );
