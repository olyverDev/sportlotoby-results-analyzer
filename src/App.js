import React, { Component } from 'react';

import { parseLotteryResultsXlsx, createLotteryBallsStats } from './util';

import './App.css';

class App extends Component {
  state = {
    lotteryBallsStats: Object.create(null),
    lotteryResults: [],
  };

  onChange = async (event) => {
    const { rows: lotteryResults } = await parseLotteryResultsXlsx(event.target.files[0]);
    const lotteryBallsStats = createLotteryBallsStats(lotteryResults);

    this.setState({ lotteryBallsStats, lotteryResults });
  };

  render() {
    const { lotteryBallsStats = Object.create(null), lotteryResults = [] } = this.state;

    return (
      <div className="App">
        <div className="App-header">
          <input
            className="App-chooseFile"
            type="file"
            onChange={this.onChange}
          />
          <div>{`Количество тиражей: ${lotteryResults.length}`}</div>
        </div>
        <table className="App-table">
          <thead>
            <tr>
              <th>Число</th>
              <th>Выпадало</th>
              <th>Выпадало (%)</th>
              <th>Последний тираж</th>
            </tr>
          </thead>
          <tbody>
            {Object.values(lotteryBallsStats).sort((a, b) => b.count - a.count).map((stats) => {
              return (
                <tr key={stats.key}>
                  <td>{stats.key}</td>
                  <td>{stats.count}</td>
                  <td>{((stats.count / lotteryResults.length) * 100).toFixed(2)}</td>
                  <td>{`${stats.last.draw} / ${stats.last.date}`}</td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
    );
  }
}

export default App;
